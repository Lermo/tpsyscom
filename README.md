# TPSiscomp

Este proyecto va a consistir en hacer con la Raspberry Pi un emulador para retrogaming.
Se seguirá como guía el proyecto que se encuentra en el siguiente link:
https://hackaday.io/project/2090-raspberry-pi-vintage-arcade

Las modificaciones que se tienen pensadas son las siguientes:

-Cambiar la configuración de arcade por un joystick USB (lo que implicaría desarrollar
un driver para el mismo o modificar uno existente).

-En vez de usar la pagina web de PiMAME OS, usaremos un cliente FTP.